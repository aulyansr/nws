namespace :db do
  namespace :populate do
    task all: [:user, :main_category, :category, :event, :article] do
    end

    task user: :environment do
      u = User.new
      u.username = "administration"
      u.role = "admin"
      u.email = "admin@bebasaja.com"
      u.password = "password"
      u.save

      u = User.new
      u.username = "Dandi"
      u.role = "writer"
      u.email = "dandi@bebasaja.com"
      u.password = "password"
      u.save
    end

    task main_category: :environment do
      Category.destroy_all
      c = Category.new
      c.title = "travel"
      c.main= true
      c.save

      c = Category.new
      c.title = "kuliner"
      c.main= true
      c.save

      c = Category.new
      c.title = "hobby"
      c.main= true
      c.save

      c = Category.new
      c.title = "music dan film"
      c.main= true
      c.save

      c = Category.new
      c.title = "ototech"
      c.main= true
      c.save

      c = Category.new
      c.title = "famili dan edukasi"
      c.main= true
      c.save

      c = Category.new
      c.title = "kesehatan dan kecantikan"
      c.main= true
      c.save

      c = Category.new
      c.title = "event"
      c.main= true
      c.save
    end

    task article: :environment do
      Article.destroy_all
      Category.main_category.each do |x|
        x.children.each do |y|
          2.times do |i|
            u = Article.new
            u.title = Faker::Book.title
            u.content = Faker::Lorem.paragraph(sentence_count: 2)
            u.metades = Faker::Lorem.paragraph(sentence_count: 20)
            u.remote_image_url = Faker::LoremFlickr.image
            u.category = y
            u.user = User.where(role: "writer").sample
            u.publish = DateTime.now
            if u.save
              puts "Article created: #{u.title}"
            else
              puts "Article error: #{u.title} -> #{u.errors.first.join(": ")}"
            end
          end
        end
      end
    end

    task event: :environment do
      Event.destroy_all
        5.times do |i|
          u = Event.new
          u.title = Faker::Book.title
          u.content = Faker::Lorem.paragraph(sentence_count: 20)
          u.metades = Faker::Lorem.sentence
          u.remote_image_url = Faker::LoremFlickr.image
          u.category = Category.event_category.sample
          u.start_date = Date.yesterday
          u.end_date = Faker::Date.forward(23)
          if u.save
            puts "Event created: #{u.title}"
          else
            puts "Event error: #{u.title} -> #{u.errors.first.join(": ")}"
          end
        end
    end

    task category: :environment do
    Category.where.not(id: Category.main_category).where.not(slug: 'event').destroy_all
    Category.main_category.each do |x|
      2.times do |i|
        u = Category.new
        u.title = Faker::Book.genre
        u.desc = Faker::Lorem.sentence
        u.parent = x
        u.main = false
        if u.save
          puts "Category created: #{u.title}"
        else
          puts "Category error: #{u.title} -> #{u.errors.first.join(": ")}"
        end
      end
    end
  end

  end
end
