class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :title
      t.references :category, foreign_key: true
      t.text :content
      t.date :start_date
      t.date :end_date
      t.string :slug
      t.string :metades
      t.string :image
      t.string :other_info

      t.timestamps
    end
    add_index :events, :slug, unique: true
  end
end
