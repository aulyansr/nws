class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :title
      t.references :category, foreign_key: true
      t.text :content
      t.datetime :publish
      t.string :slug
      t.string :metades
      t.string :image

      t.timestamps
    end
    add_index :articles, :slug, unique: true
  end
end
