class CreateDirectories < ActiveRecord::Migration[5.2]
  def change
    create_table :directories do |t|
      t.string :name
      t.string :directory_type
      t.string :description
      t.string :address
      t.string :phonenumber
      t.string :province
      t.string :city
      t.string :instagram
      t.string :facebook
      t.string :twitter
      t.string :website

      t.timestamps
    end
  end
end
