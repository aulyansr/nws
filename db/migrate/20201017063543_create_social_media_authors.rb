class CreateSocialMediaAuthors < ActiveRecord::Migration[5.2]
  def change
    create_table :social_media_authors do |t|
      t.references :user, foreign_key: true
      t.references :socialmedia, foreign_key: true
      t.string :account_username

      t.timestamps
    end
  end
end
