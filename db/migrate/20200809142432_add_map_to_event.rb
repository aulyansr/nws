class AddMapToEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :map, :text
    add_column :events, :directory_id, :string
  end
end
