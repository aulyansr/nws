class AddParentIdToArticle < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :parent_id, :string
  end
end
