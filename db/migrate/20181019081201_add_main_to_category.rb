class AddMainToCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :main, :boolean
  end
end
