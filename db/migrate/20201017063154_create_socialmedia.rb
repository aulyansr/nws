class CreateSocialmedia < ActiveRecord::Migration[5.2]
  def change
    create_table :socialmedia do |t|
      t.string :title
      t.string :link
      t.text :icon

      t.timestamps
    end
  end
end
