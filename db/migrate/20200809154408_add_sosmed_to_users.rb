class AddSosmedToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :fb, :string
    add_column :users, :ig, :string
    add_column :users, :youtube, :string
    add_column :users, :linkedin, :string
  end
end
