class CreateCategoryDirectories < ActiveRecord::Migration[5.2]
  def change
    create_table :category_directories do |t|
      t.string :title
      t.text :description
      t.string :slug

      t.timestamps
    end
  end
end
