class AddDirectoryToArticle < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :directory_id, :integer
  end
end
