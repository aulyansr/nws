class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :title
      t.text :desc
      t.string :slug
      t.integer :parent_id

      t.timestamps
    end
    add_index :categories, :slug, unique: true
  end
end
