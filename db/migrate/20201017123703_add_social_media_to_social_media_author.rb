class AddSocialMediaToSocialMediaAuthor < ActiveRecord::Migration[5.2]
  def change
    add_column :social_media_authors, :socialmedium_id, :string
  end
end
