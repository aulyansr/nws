class AddCategoryDirectoryToDirectory < ActiveRecord::Migration[5.2]
  def change
    add_column :directories, :category_directory_id, :string
  end
end
