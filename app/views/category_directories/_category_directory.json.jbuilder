json.extract! category_directory, :id, :title, :description, :slug, :created_at, :updated_at
json.url category_directory_url(category_directory, format: :json)
