json.extract! contact, :id, :fullname, :email, :subject, :phone, :message, :created_at, :updated_at
json.url contact_url(contact, format: :json)
