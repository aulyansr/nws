json.extract! directory, :id, :name, :directory_type, :description, :address, :phonenumber, :province, :city, :instagram, :facebook, :twitter, :website, :created_at, :updated_at
json.url directory_url(directory, format: :json)
