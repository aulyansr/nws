json.extract! socialmedium, :id, :title, :link, :icon, :created_at, :updated_at
json.url socialmedium_url(socialmedium, format: :json)
