json.data(@tagged) do |x|
  json.id x.id
  json.title x.title.html_safe
  json.category x.category.title
  json.url "https://bebasaja.com/" + x.category.parent.slug+"/" + x.slug
  json.image x.image.regular
end
