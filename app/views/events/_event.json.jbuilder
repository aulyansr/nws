json.extract! event, :id, :title, :category_id, :content, :start_date, :end_date, :slug, :metades, :image, :other_info, :created_at, :updated_at
json.url event_url(event, format: :json)
