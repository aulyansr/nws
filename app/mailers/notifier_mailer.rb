class NotifierMailer < ApplicationMailer
  default from: "bebasaja.com <hello@bebasaja.com>"

  def article_publish(article)
    @article = article
    mail( :to => "janoko@bebasaja.com",
      Bcc: "leo@bebasaja.com",
      :subject => "#{article.user.username} request to publish - #{article.title} ",
      :display_name => "bebasaja.com" )
  end

end
