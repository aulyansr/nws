class Event < ApplicationRecord
  extend FriendlyId
  friendly_id :title
  acts_as_taggable_on :tags
  ActsAsTaggableOn.delimiter = ','

  belongs_to :user, optional: true
  belongs_to :category
  belongs_to :directory, optional: true
  mount_uploader :image, ImageUploader

  Status = [ "publish", "draft" ]

  class << self
    def publish
      Event.where(status: "publish")
    end

    def up_coming
      Event.publish.where("events.end_date >=?", DateTime.now).order("start_date asc")
    end

    def on_going
      Event.publish.where("events.end_date >= ?", DateTime.now)
    end
  end

  def self.search(search)
    where("title ILIKE ? OR metades ILIKE ?", "%#{search}%", "%#{search}%")
  end
end
