class Category < ApplicationRecord
  extend FriendlyId
  friendly_id :title

  has_many :articles
  has_many :events

  has_many :children, :class_name => "Category", :foreign_key => :parent_id
  belongs_to :parent, :class_name => "Category", :foreign_key => :parent_id, optional: true

  class << self
    def main_category
      Category.where(main: true)
    end

    def article_category
       Category.where.not(main: true).where.not(id: Category.event_category)
    end

    def event_category
      Category.where(slug:'event').first.children
    end

    def parent
      Category.where(parent_id: [nil,""])
    end
  end
end
