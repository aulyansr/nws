class User < ApplicationRecord
  extend FriendlyId
  friendly_id :username
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,:recoverable, :rememberable, :validatable

  has_many :articles
  has_many :events
  has_many :social_media_authors, inverse_of: :user
  accepts_nested_attributes_for :social_media_authors, reject_if: :all_blank, allow_destroy: true
  mount_uploader :avatar, ImageUploader

  ROLES = {writer: "writer", admin:'admin'}

  scope :admin, -> {where(role: ROLES[:admin])}
  scope :writer, -> {where(role: ROLES[:writer])}

  def is_admin?
   self.role == ROLES[:admin]
 end

 def is_writer?
   self.role == ROLES[:writer]
 end

end
