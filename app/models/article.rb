class Article < ApplicationRecord
  extend FriendlyId
  acts_as_taggable_on :tags
  ActsAsTaggableOn.delimiter = ','
  friendly_id :title

  belongs_to :category
  belongs_to :directory, optional: true
  belongs_to :user

  mount_uploader :image, ImageUploader

  StatusWriter = [ "publish", "draft", "request publish" ]
  Status = [ "publish", "draft", "need revision" ]


  def increase_view!
    self.view_count = self.view_count.to_i + 1
    self.save
  end

  def draft?
    self.status == "draft"
  end

  class << self

    def publish
      Article.where(status: "publish").where("articles.publish <= ?", DateTime.now).order(publish: :desc)
    end

    def popular
      Article.reorder(view_count: :desc)
    end
    def wisata
      Article.publish.where(category_id: Category.where(slug: "wisata").first.children).order("publish desc")
    end
    def kuliner
      Article.publish.where(category_id: Category.where(slug: "kuliner").first.children).order("publish desc")
    end
    def hobby
      Article.publish.where(category_id: Category.where(slug: "hobi").first.children).order("publish desc")
    end
    def musik_dan_film
      Article.publish.where(category_id: Category.where(slug: "musik-dan-film").first.children).order("publish desc")
    end
    def ototech
      Article.publish.where(category_id: Category.where(slug: "ototech").first.children).order("publish desc")
    end
    def famili_dan_edukasi
      Article.publish.where(category_id: Category.where(slug: "famili-dan-edukasi").first.children).order("publish desc")
    end
    def gaya_hidup
      Article.publish.where(category_id: Category.where(slug: "gaya-hidup").first.children).order("publish desc")
    end

    def ragam_budaya
      Article.publish.where(category_id: Category.where(slug: "ragam-budaya").first.children).order("publish desc")
    end

    def event
      Article.publish.where(category_id: Category.where(slug: "event").first.children).order("publish desc")
    end
  end
  def self.search(search)
    where("title ILIKE ? OR metades ILIKE ?", "%#{search}%", "%#{search}%")
  end
end
