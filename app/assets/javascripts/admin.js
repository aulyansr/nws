// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require admin/jquery
//= require rails-ujs
//= require activestorage
// require turbolinks
//= require ckeditor/loader
//= require moment
//= require public/bootstrap.min
//= require dataTables/jquery.dataTables
//= require admin/popper.min
//= require admin/dragula.min
//= require admin/app-style-switcher.horizontal
//= require admin/app.init.mini-sidebar
//= require admin/app
//= require admin/perfect-scrollbar.jquery.min
//= require admin/sidebarmenu
//= require admin/waves
//= require admin/datatables.min
//= require admin/bootstrap-switch.min
//= require admin/sparkline
//= require admin/jquery.sortable
//= require admin/dropify
//= require admin/bootstrap-material-datetimepicker
//= require select2
//= require selectize
//= require cocoon
//= require admin/custom
