$(function() {

$(document).scroll(function(){
  var $nav = $("#small-logo ");
  var $ht = $("#home-text");
  $nav.toggleClass('d-block', $(this).scrollTop() > $nav.height());
  $ht.toggleClass('d-none', $(this).scrollTop() > $nav.height());
});


$( "#search-triger").click(function() {
  $("#search-bar").removeClass('d-none');
  $("#navbar").removeClass('visible');
  $("#navbar").addClass('invisible');
  $('.search-input').focus();
});

$( "#form-close").click(function() {
  $("#search-bar").addClass('d-none');
  $("#navbar").addClass('visible');
  $("#navbar").removeClass('invisible');
});



});
