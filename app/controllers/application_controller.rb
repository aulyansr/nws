class ApplicationController < ActionController::Base
   before_action :prepare_meta_tags
   def prepare_meta_tags(options={})
    site_name   = "Bebasaja"
    title       = "Informasi Bebas dan Bermanfaat"
    description = "Bebasaja merupakan website bacaan ringan yang memberikan informasi dan rekomendasi mengenai tempat wisata, kuliner, gaya hidup, event, dan hal lainnya yang menarik."
    image       =  "https://bebasaja.com/assets/main-logo.png"
    current_url = "http://bebasaja.com/"

    # Let's prepare a nice set of defaults
    defaults = {
      reverse: true,
      site:        site_name,
      title:       title,
      image:       image,
      description: description,
      twitter: {
        site_name: site_name,
        site: '@bebasajacom',
        creator: '@bebasajacom',
        card: 'summary_large_image',
        description: description,
        title: title,
        image: image
      },
      og: {
        url: current_url,
        site_name: site_name,
        title: title,
        image: image,
        description: description,
        type: 'website'
      }
    }

    options.reverse_merge!(defaults)

    set_meta_tags options

  end
end
