class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  layout 'admin', except: 'show'
  before_action :authenticate_user!, except: 'show'
  # GET /articles
  # GET /articles.json
  def index
    if current_user.is_admin?
      @articles = Article.all
    else
      @articles = Article.where(user_id: current_user.id)
    end



  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    if params[:category_name] == nil
      redirect_to dtl_article_path(@article.category.parent.slug, @article.slug), status: 301
    end
    if @article.draft?
      unless user_signed_in?
        redirect_to root_path
      end
    end
    @article.increase_view!
    category_name = @article.category.parent.slug
    @related = Article.where.not(id: @article.id).where(category_id: @article.category_id).order("RANDOM()").limit(3)
    prepare_meta_tags(
      title: @article.title,
      description: @article.metades,
      image: @article.image,
      twitter: {
        site_name: "Bebasaja | #{@article.title || @article.title}" ,
        site: '@bebasajacom',
        creator: '@bebasajacom',
        card: 'summary',
        description:   @article.metades || ActionView::Base.full_sanitizer.sanitize(@article.content).truncate(250),
        title: @article.title || @article.title,
        image: request.protocol + request.host_with_port + @article.image.url,
        url: request.protocol + request.host_with_port+"/"+ @article.category.parent.slug + "/" +  @article.slug ,
      },
      og: {
        url: request.protocol + request.host_with_port+"/"+ @article.category.parent.slug + "/" +  @article.slug ,
        site_name:  "Bebasaja | #{@article.title || @article.title}" ,
        title: @article.title || @article.title,
        image: request.protocol + request.host_with_port + @article.image.url,
        type: 'article',
        description:  @article.metades || ActionView::Base.full_sanitizer.sanitize(@article.content).truncate(250)
        })
      end

      # GET /articles/new
      def new
        @article = Article.new(publish: DateTime.now)
        @category = Category.new
        @directory = Directory.new
      end

      # GET /articles/1/edit
      def edit
        @category = Category.new
        @directory = Directory.new
      end

      # POST /articles
      # POST /articles.json
      def create
        @article = Article.new(article_params)
        if params[:preview].present?
          params[:article][:status] = "draft"
        else
          params[:article][:status] = "publish"
        end

        respond_to do |format|
          if @article.save
            unless current_user.is_admin?
              if @article.status == 'request publish'
                NotifierMailer.article_publish(@article).deliver
              end
            end
            format.html { redirect_to @article, notice: 'Article was successfully created.' }
            format.json { render :show, status: :created, location: @article }
          else
            format.html { render :new }
            format.json { render json: @article.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /articles/1
      # PATCH/PUT /articles/1.json
      def update
        respond_to do |format|
          if params[:draft].present?
            params[:article][:status] = "draft"
          else
            params[:article][:status] = "publish"
          end
          if @article.update(article_params)
            format.html { redirect_to @article, notice: 'Article was successfully updated.' }
            unless current_user.is_admin?
              if @article.status == 'request publish'
                NotifierMailer.article_publish(@article).deliver
              end
            end
            format.json { render :show, status: :ok, location: @article }
          else
            format.html { render :edit }
            format.json { render json: @article.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /articles/1
      # DELETE /articles/1.json
      def destroy
        @article.destroy
        respond_to do |format|
          format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_article
        @article = Article.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def article_params
        params.require(:article).permit(:title, :category_id, :content, :publish, :slug, :metades, :image, :status, :tag_list, :parent_id, :user_id, :directory_id)
      end
    end
