class CategoryDirectoriesController < ApplicationController
  before_action :set_category_directory, only: [:show, :edit, :update, :destroy]
  layout 'admin', except: 'show'
  before_action :authenticate_user!
  # GET /category_directories
  # GET /category_directories.json
  def index
    @category_directories = CategoryDirectory.all
  end

  # GET /category_directories/1
  # GET /category_directories/1.json
  def show
  end

  # GET /category_directories/new
  def new
    @category_directory = CategoryDirectory.new
  end

  # GET /category_directories/1/edit
  def edit
  end

  # POST /category_directories
  # POST /category_directories.json
  def create
    @category_directory = CategoryDirectory.new(category_directory_params)

    respond_to do |format|
      if @category_directory.save
        format.html { redirect_to category_directories_url, notice: 'Category directory was successfully created.' }
        format.json { render :show, status: :created, location: @category_directory }
      else
        format.html { render :new }
        format.json { render json: @category_directory.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /category_directories/1
  # PATCH/PUT /category_directories/1.json
  def update
    respond_to do |format|
      if @category_directory.update(category_directory_params)
        format.html { redirect_to category_directories_url, notice: 'Category directory was successfully updated.' }
        format.json { render :show, status: :ok, location: @category_directory }
      else
        format.html { render :edit }
        format.json { render json: @category_directory.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /category_directories/1
  # DELETE /category_directories/1.json
  def destroy
    @category_directory.destroy
    respond_to do |format|
      format.html { redirect_to category_directories_url, notice: 'Category directory was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category_directory
      @category_directory = CategoryDirectory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_directory_params
      params.require(:category_directory).permit(:title, :description, :slug)
    end
end
