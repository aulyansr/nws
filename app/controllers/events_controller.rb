class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  layout 'admin', except: 'show'
  before_action :authenticate_user!, except: 'show'

  # GET /events
  # GET /events.json
  def index
    @events = Event.all
    if current_user.is_admin?
      @events = Event.all
    else
      @events = Event.where(user_id: current_user.id)
    end

  end

  # GET /events/1
  # GET /events/1.json
  def show
    @related = Event.where.not(id: @event.id).order(end_date: :asc).limit(4)
    prepare_meta_tags(
      title: @event.title,
      description: @event.metades || @event.content.html_safe.truncate(160),
      image: @event.image,
      twitter: {
        site_name: "Bebas Aja | #{@event.title}" ,
        site: '@bebasajacom',
        creator: '@bebasajacom',
        card: 'summary',
        description: @event.metades || ActionView::Base.full_sanitizer.sanitize(@event.content).truncate(250),
        title: @event.title,
        image: @event.image.url
      },
      og: {
        url: request.protocol + request.host_with_port+"/events/"+ @event.slug,
        site_name:  "Bebas Aja | #{@event.title || @event.title}" ,
        title: @event.title,
        image: @event.image.url,
        type: 'event',
        description:  @event.metades || ActionView::Base.full_sanitizer.sanitize(@event.content).truncate(250)
        })
    
  end

  # GET /events/new
  def new
    @event = Event.new
    @category = Category.new
    @directory = Directory.new
  end

  # GET /events/1/edit
  def edit
    @category = Category.new
    @directory = Directory.new
  end

  # POST /events
  # POST /events.json
  def create
    if params[:preview].present?
      params[:event][:status] = "draft"
    else
      params[:event][:status] = "publish"
    end
    @event = Event.new(event_params)

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    if params[:preview].present?
      params[:event][:status] = "draft"
    else
      params[:event][:status] = "publish"
    end
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_event
    @event = Event.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def event_params
    params.require(:event).permit(:title, :category_id, :content, :start_date, :end_date, :venue, :slug, :map, :metades, :image, :other_info, :tag_list, :directory_id, :status)
  end
end
