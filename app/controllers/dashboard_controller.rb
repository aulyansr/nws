class DashboardController < ApplicationController
  layout 'admin', except: 'show'
  before_action :authenticate_user!
  def index
    @drft_articles = Article.where(status: "draft")
    @on_schedule = Article.where(status: "publish").where("articles.publish >= ?", DateTime.now)
    @waiting = Article.where(status: "pending")
    if current_user.is_writer?
      @waiting = @waiting.where(user_id: current_user.id)
    end
  end

  def backtosetup
    if params[:id].present?
      redirect_to edit_article_path(params[:id])
    else
      redirect_to edit_article_path(Article.all.order(id: :desc).first.id)
    end
  end

  def backtosetup_event
    redirect_to edit_event_path(Event.all.order(id: :desc).first.id)
  end

  def banner_homepage
    # code
  end

  def banner_category
      @categories = Category.all
  end

  def banner_article
      @categories = Category.all
  end

  def search
    category = Category.where("title like  ?", "%#{search}%").map{}
    @search = Article.where("title like  ?", "%#{search}%")
  end
end
