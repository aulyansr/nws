class PageController < ApplicationController
  def index
    @wisata = Article.wisata.first
    @kuliner = Article.kuliner.first
    @hobby = Article.hobby.first
    @music_dan_film = Article.musik_dan_film.first
    @ototech = Article.ototech.first
    @famili_dan_edukasi = Article.famili_dan_edukasi.first
    @gaya_hidup = Article.gaya_hidup.first
    @ragam_budaya  = Article.ragam_budaya.first

    @video = Video.all.limit(10)
    @latest = Article.where.not(id: [@wisata.id,@kuliner.id,@hobby.id,@music_dan_film.id,@ototech.id,@famili_dan_edukasi.id, @ragam_budaya.id, @gaya_hidup.id]).order("publish desc").limit(5)
    if params[:date].present?
      @event = Event.where("events.end_date >=?", params[:date]).order("start_date asc").limit(5)
    else
      @event = Event.up_coming.limit(5).order("start_date asc")
    end

    respond_to do |format|
      format.html
      format.js
    end
  end

  def latest
    @latest = Article.publish.order("publish desc").page(params[:page]).per(6)
  end

  def event
    @events = Event.up_coming.all.order("start_date asc")
    @months = Event.up_coming.order(end_date: :asc)
    if  params[:category].present?
        if  params[:category] ==  "all"
          @events = @events
        else
          @events = @events.where(category_id: Category.find(params[:category]))
      end
    end
    if params[:month].present?
        if params[:month] == "all"
          @events = @events
        else
          @events = @events.by_month( params[:month], field: :end_date)
        end
    end
  end

  def search
    @article = Article.publish.search(params[:search]).map(&:id)
    @tagged  = Article.publish.tagged_with(params[:search]).map(&:id)
    @article |= @tagged
    @search_result_article = Article.where(id: @article)
    @events = Event.search(params[:search]).map(&:id)
    @tagged_events = Event.tagged_with(params[:search]).map(&:id)
    @events |= @tagged_events
    @search_result_event = Event.where(id: @events)


  end

  def open
    @tagged = Article.publish.all.first(3)
  end

  def contact
    @contact = Contact.new
  end

  def author
    # code
  end

end
