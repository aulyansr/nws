class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  layout 'admin', except: 'show'
  before_action :authenticate_user!, except: 'show'
  # GET /categories
  # GET /categories.json
  def index
    @categories = Category.all
    if params[:title].present?
     @categories = Category.where("title LIKE ?", "%#{params[:title]}%")
   end

  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    if params[:category].present?
      @first_five = Article.publish.where(category_id: Category.find(params[:category]).id).order(publish: :desc).order(publish: :desc).limit(5)
      @all_article =  Article.publish.where(category_id: Category.find(params[:category])).where.not(id:  @first_five.map(&:id) ).order(publish: :desc).page(params[:page]).per(4)
    else
      @first_five = Article.publish.where(category_id: @category.children.map(&:id)).order(publish: :desc).limit(5)
      @all_article =  Article.publish.where(category_id: @category.children.map(&:id)).where.not(id:  @first_five.map(&:id) ).order(publish: :desc).page(params[:page]).per(4)
    end

    prepare_meta_tags(
    title: @category.title,
    description: @category.desc,
      twitter: {
      site_name: "BEBAS AJA | #{@category.title || @category.title}" ,
      site: '@bebasajacom',
      creator: '@bebasajacom',
      card: 'summary',
      description:   @category.desc,
      title: @category.title || @category.title,
    },
    og: {
      url: request.protocol + request.host_with_port+"/"+ @category.slug,
      site_name:  "BEBAS AJA | #{@category.title || @category.title}" ,
      title: @category.title || @category.title,
      type: 'category',
      description:  @category.desc
      })
  end

  # GET /categories/new
  def new
    @category = Category.new
  end

  # GET /categories/1/edit
  def edit
  end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        format.html { redirect_to all_category_path, notice: 'Category was successfully created.' }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to all_category_path, notice: 'Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category.destroy
    respond_to do |format|
      format.html { redirect_to all_category_path, notice: 'Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def article
    @categories = Category.where.not(main: true).where.not(id: Category.event_category)
    if params[:title].present?
      @categories =  @categories.where("title LIKE ?", "%#{params[:title]}%")
    end
  end

  def event
    @categories = Category.event_category
    if params[:title].present?
      @categories =  @categories.where("title LIKE ?", "%#{params[:title]}%")
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_category
    @category = Category.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def category_params
    params.require(:category).permit(:title, :desc, :parent_id, :main, :slug, :created_at, :index, :map, :directory_id, :venue)
  end
end
