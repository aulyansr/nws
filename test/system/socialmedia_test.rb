require "application_system_test_case"

class SocialmediaTest < ApplicationSystemTestCase
  setup do
    @socialmedium = socialmedia(:one)
  end

  test "visiting the index" do
    visit socialmedia_url
    assert_selector "h1", text: "Socialmedia"
  end

  test "creating a Socialmedium" do
    visit socialmedia_url
    click_on "New Socialmedium"

    fill_in "Icon", with: @socialmedium.icon
    fill_in "Link", with: @socialmedium.link
    fill_in "Title", with: @socialmedium.title
    click_on "Create Socialmedium"

    assert_text "Socialmedium was successfully created"
    click_on "Back"
  end

  test "updating a Socialmedium" do
    visit socialmedia_url
    click_on "Edit", match: :first

    fill_in "Icon", with: @socialmedium.icon
    fill_in "Link", with: @socialmedium.link
    fill_in "Title", with: @socialmedium.title
    click_on "Update Socialmedium"

    assert_text "Socialmedium was successfully updated"
    click_on "Back"
  end

  test "destroying a Socialmedium" do
    visit socialmedia_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Socialmedium was successfully destroyed"
  end
end
