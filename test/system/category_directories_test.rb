require "application_system_test_case"

class CategoryDirectoriesTest < ApplicationSystemTestCase
  setup do
    @category_directory = category_directories(:one)
  end

  test "visiting the index" do
    visit category_directories_url
    assert_selector "h1", text: "Category Directories"
  end

  test "creating a Category directory" do
    visit category_directories_url
    click_on "New Category Directory"

    fill_in "Description", with: @category_directory.description
    fill_in "Slug", with: @category_directory.slug
    fill_in "Title", with: @category_directory.title
    click_on "Create Category directory"

    assert_text "Category directory was successfully created"
    click_on "Back"
  end

  test "updating a Category directory" do
    visit category_directories_url
    click_on "Edit", match: :first

    fill_in "Description", with: @category_directory.description
    fill_in "Slug", with: @category_directory.slug
    fill_in "Title", with: @category_directory.title
    click_on "Update Category directory"

    assert_text "Category directory was successfully updated"
    click_on "Back"
  end

  test "destroying a Category directory" do
    visit category_directories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Category directory was successfully destroyed"
  end
end
