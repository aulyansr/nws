require 'test_helper'

class CategoryDirectoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @category_directory = category_directories(:one)
  end

  test "should get index" do
    get category_directories_url
    assert_response :success
  end

  test "should get new" do
    get new_category_directory_url
    assert_response :success
  end

  test "should create category_directory" do
    assert_difference('CategoryDirectory.count') do
      post category_directories_url, params: { category_directory: { description: @category_directory.description, slug: @category_directory.slug, title: @category_directory.title } }
    end

    assert_redirected_to category_directory_url(CategoryDirectory.last)
  end

  test "should show category_directory" do
    get category_directory_url(@category_directory)
    assert_response :success
  end

  test "should get edit" do
    get edit_category_directory_url(@category_directory)
    assert_response :success
  end

  test "should update category_directory" do
    patch category_directory_url(@category_directory), params: { category_directory: { description: @category_directory.description, slug: @category_directory.slug, title: @category_directory.title } }
    assert_redirected_to category_directory_url(@category_directory)
  end

  test "should destroy category_directory" do
    assert_difference('CategoryDirectory.count', -1) do
      delete category_directory_url(@category_directory)
    end

    assert_redirected_to category_directories_url
  end
end
