require 'test_helper'

class SocialmediaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @socialmedium = socialmedia(:one)
  end

  test "should get index" do
    get socialmedia_url
    assert_response :success
  end

  test "should get new" do
    get new_socialmedium_url
    assert_response :success
  end

  test "should create socialmedium" do
    assert_difference('Socialmedium.count') do
      post socialmedia_url, params: { socialmedium: { icon: @socialmedium.icon, link: @socialmedium.link, title: @socialmedium.title } }
    end

    assert_redirected_to socialmedium_url(Socialmedium.last)
  end

  test "should show socialmedium" do
    get socialmedium_url(@socialmedium)
    assert_response :success
  end

  test "should get edit" do
    get edit_socialmedium_url(@socialmedium)
    assert_response :success
  end

  test "should update socialmedium" do
    patch socialmedium_url(@socialmedium), params: { socialmedium: { icon: @socialmedium.icon, link: @socialmedium.link, title: @socialmedium.title } }
    assert_redirected_to socialmedium_url(@socialmedium)
  end

  test "should destroy socialmedium" do
    assert_difference('Socialmedium.count', -1) do
      delete socialmedium_url(@socialmedium)
    end

    assert_redirected_to socialmedia_url
  end
end
