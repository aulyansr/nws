Rails.application.routes.draw do
  resources :socialmedia
  resources :category_directories
  resources :contacts
  resources :directories
  get 'dashboard/index'
  mount Ckeditor::Engine => '/ckeditor'
  root 'page#index'
  get 'dashboard' => "dashboard#index"
  get '/search' => "page#search", as: :search_path
  get '/dashboard/banner-homepage' => "dashboard#banner_homepage", as: :banner_homepage
  get '/dashboard/banner-category' => "dashboard#banner_category", as: :banner_category
  get '/dashboard/banner-article' => "dashboard#banner_article", as: :banner_article
  get 'backtosetup' => "dashboard#backtosetup"
  get 'backtosetup-event' => "dashboard#backtosetup_event"
  get 'open' => 'page#open', as: :open_article
  get 'contact-us'=> 'page#contact', as: :contact_us

  resources :users
  resources :videos
  resources :events
  resources :articles

  get 'index' => 'page#index', as: :index
  get 'event' => 'page#event', as: :page_event
  get 'all-category' => 'categories#index', as: :all_category
  get 'all-category-article' => 'categories#article', as: :all_category_article
  get 'all-category-event' => 'categories#event', as: :all_category_event
  get 'all-directory' => 'directories#index', as: :all_directory
  get 'semua-artikel' => 'page#latest', as: :latest
  get 'author' => 'page#author', as: :all_author

  resources :categories, path: ''

  devise_for :users,
  path:'admin',
  path_names:{sign_in: 'login',sign_out:'logout'}

  get 'author/:id', to: 'users#show', as: :author
  get ':category_name/:id', to: 'articles#show', as: :dtl_article

end
