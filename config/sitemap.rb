SitemapGenerator::Sitemap.default_host = "https://bebasaja.com/"

SitemapGenerator::Sitemap.create do
  add root_path, :changefreq => 'daily'
  add latest_path, :changefreq => 'daily'
  add page_event_path, :changefreq => 'daily'

  Category.main_category.each do |x|
    add  category_path(x.slug), :changefreq => 'monthly', :lastmod => x.updated_at
  end

  Event.all.each do |x|
    add  event_path(x.slug), :changefreq => 'monthly', :lastmod => x.updated_at
  end

  Article.publish.each do |x|
    add  dtl_article_path(x.category.parent.slug, x.slug), :changefreq => 'daily', :lastmod => x.updated_at
  end
end
